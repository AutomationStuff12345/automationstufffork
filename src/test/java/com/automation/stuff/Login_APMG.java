package com.automation.stuff;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Login_APMG {
	WebDriver driver = null;

	@Test
	public void testGoogle(){
		System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"//Drivers//chromedriver.exe");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://quarto-dev-ui.azurewebsites.net/login");

		//UserName verification and enter values
		if(driver.findElement(By.name("userName")).isDisplayed()){
			if(driver.findElement(By.name("userName")).isEnabled()){
				WebElement Username = driver.findElement(By.name("userName"));
				Username.sendKeys("jppatel@codal.com");
			}else System.out.println("UserName field is not enable");
		}else System.out.println("UserName field is not displayed");
		System.out.println("UserName entered successfully");

		//Password verification and enter values
		if(driver.findElement(By.name("password")).isDisplayed()){
			if(driver.findElement(By.name("password")).isEnabled()){
				WebElement password = driver.findElement(By.name("password"));
				password.sendKeys("password");
			}else System.out.println("password field is not enable");
		}else System.out.println("password field is not displayed");
		System.out.println("Password entered successfully");

		WebElement Login = driver.findElement(By.cssSelector("#content-area > div > div > div > div.card > div > form > div.d-flex.justify-content-end.pt-24 > div > button"));
		Login.click();
		System.out.println("Login into the portal");
	}
}
